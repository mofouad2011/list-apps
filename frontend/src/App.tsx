import React, {Component} from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {Container} from "reactstrap";

import HeaderMenu from "./components/HeaderMenu";
import ListingApps from "./components/ListApps";
import AddNewApp from "./components/AddNewAppForm";
import SingleApp from "./components/SingleApp";


class App extends Component {
 

  render() {
    return (
      <Container className="App">
        <Router>
          <HeaderMenu />
          <Switch>
            <Route exact path="/" component={ListingApps} />
            <Route exact path="/add-new-app" component={AddNewApp} />
            <Route exact path="/api/:app" component={SingleApp} />
          </Switch>
        </Router>
      </Container>
    );
  }
}

export default App;
