import React from "react";
import axios from "axios";
import {Alert, Button, Form, FormGroup, Input, Table} from "reactstrap";
import API from '../Helpers/APICalls'


interface ISingleAppProps {
    id: any;
}
interface IVersionsObj {
    [id: string]: { id: string; file: string };
}
interface ISingleAppState {
    appId: string;
    appName: string;
    appVersions: IVersionsObj
    AppUploadedFile: string
    AppUploadedVersion: string
}
interface MatchParams {
    match: any;
}

class SingleApp extends React.Component<
    ISingleAppProps & MatchParams,
    ISingleAppState
> {
    constructor(props: ISingleAppProps & MatchParams) {
        super(props);
    }

    state: ISingleAppState = {
        appId: "",
        appName: "",
        appVersions: {},

        // Uploaded Form 
        AppUploadedFile: "",
        AppUploadedVersion: ""
        
    };

    async componentDidMount() {
        await API.getAppByID(this.props.match.params.app).then(res => {
             
                if (res.status === 200) {
                    this.setState({
                        appId: res.data.id,
                        appName: res.data.name,
                        appVersions: res.data.versions
                    });
                }
            });
    }


    private deleteFile = (e: React.MouseEvent<HTMLElement>, appVer: string) => {
        const {appId} = this.state;

        API.deleteAppVersion(appId, appVer).then(res => {
            if (res.status === 200) {
                let newStat = {...this.state.appVersions};
                delete newStat[appVer];
                this.setState({appVersions: newStat}); 
            }
        });
    };

    private addNewVersion = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        // upload New File & version to the API

        const {appId, AppUploadedVersion, AppUploadedFile} = this.state;
        const data = new FormData()
        data.append('file', AppUploadedFile);

        await axios.post(`/api/apps/${appId}/${AppUploadedVersion}`)
            .then(res => {

                // todo : apstract the APIS
                if (res.status === 200) {
                    axios.post(`/api/apps/${appId}/${AppUploadedVersion}/file`, data, {
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    }).then(res => {
                        if (res.status === 200) {

                            // Getting data from the post and store it in the state
                            axios.get(`/api/apps/${appId}`).then(res => {
                                if (res.status === 200) {
                                    let {versions} = res.data;
                                    this.setState({
                                        appVersions: versions,
                                        AppUploadedVersion: "",
                                        AppUploadedFile: ""
                                    });
                                }
                            })
                        }
                    })
                }
            })
    };

    private fileHasUploaded = (e: React.ChangeEvent<HTMLInputElement>) => {
        // Make sure that we have a file in our Form
        const {files} = e.target;
        // @ts-ignore
        if (files[0]) this.setState({AppUploadedFile: files[0]})
    };

    private appVersionHasInserted = (e: React.FormEvent<HTMLInputElement>) => {
        // Make sure that we inserted a version
        this.setState({AppUploadedVersion: e.currentTarget.value})
    };
    

    public render() {
        const AppName = this.state.appName;
        const appVer = this.state.appVersions;

        const versions: Array<any> = Object.keys(appVer).reduce((acc, key) => {
            //@ts-ignore
            acc.push(appVer[key]);
            return acc;
        }, []);

        return (
            <div>
                <h3>
                    Application Name : <b>{AppName}</b>
                </h3>

                <hr/>

                {versions.length === 0 ? (
                    <div>There is No App Versions yet,, but you can add :)</div>
                ) : (
                    <Table>
                        <thead>
                        <tr>
                            <th>App Version</th>
                            <th>App File <small>*exe</small></th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {versions.map(ele => (
                            <tr key={ele.id}>
                                <td>{ele.id}</td>
                                <td>
                                    {ele.file && (
                                        <a href={ele.file} download>
                                            {ele.file.split("/")[3]}
                                        </a>
                                    )}
                                </td>
                                <td>
                                    <Button
                                        color="danger"
                                        onClick={e => {
                                            this.deleteFile(e, ele.id);
                                        }}
                                    >
                                        delete
                                    </Button>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                )}
                
                
                <hr/>

                <h5>Add New Version</h5>
                <Form onSubmit={this.addNewVersion}>
                    {/*todo 
                        : Adding function if the version is Already exist or there is an error 
                        : add function if the version has uploaded
                     */}
                    <Alert color="success" isOpen={false}>
                        Your App has been added
                    </Alert>
                    <Alert color="danger" isOpen={false}>
                        This app already Exists
                    </Alert>
                    <FormGroup> 
                        <Input
                            required
                            type="text"
                            name="text"
                            id="app1"
                            value={this.state.AppUploadedVersion}
                            placeholder="Virsion Number"
                            onChange={e => this.appVersionHasInserted(e)}
                        />
                        <Input
                            required
                            type="file"
                            onChange={e => this.fileHasUploaded(e)}
                            name="appFile"
                            accept=".exe"
                            color="primary" 
                        > </Input>
                    </FormGroup>
                    <FormGroup>
                        <Button type="submit"> Add Aew Version</Button>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

export default SingleApp;
