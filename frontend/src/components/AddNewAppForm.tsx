import React from "react";
import axios from "axios";
import {Alert, Button, Col, Form, FormGroup, Input, Label, Row} from "reactstrap";

interface IAddNewAppProps {
    // if we are going to have any props in the futuer
}

interface IAddNewAppState {
    appName: string;
    appAlreadyThere: boolean;
    appHasAdded: boolean;
}

class AddNewApp extends React.Component<IAddNewAppProps, IAddNewAppState> {
    state: IAddNewAppState = {
        appName: "",
        appHasAdded: false,
        appAlreadyThere: false
    };

    public render() {
        // Passing App Name from State to Here

        const {appName, appHasAdded, appAlreadyThere} = this.state;
        return (
            <Row>
                <Col md="6">
                    <h5>Adding New App</h5>
                    <hr/>
                    <Form onSubmit={this.handleSubmit}>
                        <Alert color="success" isOpen={appHasAdded}>
                            Your App has been added
                        </Alert>
                        <Alert color="danger" isOpen={appAlreadyThere}>
                            This app already Exists
                        </Alert>
                        <FormGroup>
                            <Label for="exampleEmail">App name</Label>
                            <Input
                                required
                                type="text"
                                name="text"
                                id="app1"
                                placeholder="app1"
                                value={appName}
                                onChange={this.handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Button type="submit"> Add Aew App</Button>
                        </FormGroup>
                    </Form>
                </Col>
            </Row>
        );
    }

    private handleChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({appName: e.currentTarget.value});
    };

    private handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        let appName = this.state.appName;
        let appId = this.state.appName.replace(/\s+/g, "-").toLowerCase();
        axios
            .post(
                "/api/apps/" + appId,
                {
                    name: appName
                },
                {
                    headers: {
                        "Content-Type": "application/json"
                    }
                }
            )
            .then(res => {
                if (res.status === 200) {
                    this.setState(
                        {
                            appName: "",
                            appHasAdded: true
                        },
                        () => {
                            window.setTimeout(() => {
                                this.setState({appHasAdded: false});
                            }, 2000);
                        }
                    );
                }
            })
            .catch(err => {
                console.log(err);
                if (err.response.status === 400) {
                    this.setState(
                        {
                            appName: "",
                            appAlreadyThere: true
                        },
                        () => {
                            window.setTimeout(() => {
                                this.setState({appAlreadyThere: false});
                            }, 2000);
                        }
                    );
                }
            });
    };
}

export default AddNewApp;
