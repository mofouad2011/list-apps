import React from "react";
import { Link } from "react-router-dom";
import { Navbar, NavbarBrand, Nav, NavItem,NavLink } from "reactstrap";

const HeaderMenu = () => (
  <Navbar color="light" light expand="md" className="mb-2">
    <NavbarBrand href="/">Applications Listing App</NavbarBrand>
    <Nav className="ml-auto">
      <NavItem>
        <NavLink tag={Link} to="/">Home</NavLink>
      </NavItem>
      <NavItem>
        <NavLink tag={Link} to="/add-new-app">Add New App</NavLink>
      </NavItem>
    </Nav>
  </Navbar>
);

export default HeaderMenu;
