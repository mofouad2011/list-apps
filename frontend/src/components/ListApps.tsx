import React from "react";
import axios from "axios";
import {Link} from "react-router-dom";
import {Button, Table} from "reactstrap";

import API from '../Helpers/APICalls';

interface IListingAppsStatus {
    apps: Array<any>;
}

interface IListingAppsProps {}

class ListingApps extends React.Component<
    IListingAppsProps,
    IListingAppsStatus
> {
    state: IListingAppsStatus = {
        apps: []
    };

    async componentWillMount() {
        await axios.get("/api/apps").then(res => {
            if (res.status === 200) {
                this.setState({ apps: res.data });
            }
        });
    }

    private deleteThisApp = (e: React.MouseEvent<HTMLElement>, appId: string) => {
        API.deleteAppByID(appId).then(res => {
            if (res.status === 200) {
                let filteredArray = this.state.apps.filter(app => app.id !== appId);
                this.setState({apps: filteredArray});
            }
        })
    };
    
    public render() {
        const appsArray = this.state.apps;
        const listApps = appsArray.map(app => (
            <li key={app.id}>
                <span> - </span>
            </li>
        ));

        return (
            <div>
                <h1> Listing Apps </h1>

                {appsArray.length === 0 ? (
                    <div>You dont have any apps yet,, please go to This <Link to={"/add-new-app"}>Page</Link> to add apps</div>
                    ) : (
                    <Table>
                        <thead>
                        <tr>
                            <th>App Name</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        {appsArray.map(app => (
                            <tr key={app.id}>
                                <td><Link to={"/api/" + app.id}>{app.name} </Link></td>
                                <td><Button onClick={(e) => this.deleteThisApp(e, app.id)}>Delete this app</Button></td>
                            </tr>
                        ))}

                        </tbody>
                    </Table> 
                    )}
                
                
            </div>
        );
    }
}

export default ListingApps;
