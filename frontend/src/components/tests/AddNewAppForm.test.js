import React from 'react';
import { shallow, mount } from 'enzyme';
import ReactDOM from 'react-dom';

import AddNewApp from '../AddNewAppForm';
import {Alert, Button, Col, Form, FormGroup, Input, Label, Row} from "reactstrap";

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<AddNewApp />, div);
    ReactDOM.unmountComponentAtNode(div);
});

it('should Render Form  ', () => {
    const wrapper = shallow(<AddNewApp />); 
    expect(wrapper.find(Form)).toHaveLength(1);
    expect(wrapper.find(Alert)).toHaveLength(2);
    expect(wrapper.find(Button)).toHaveLength(1);
    expect(wrapper.find("Button[type='submit']")).toHaveLength(1);
    expect(wrapper.find(Input)).toHaveLength(1);
});

it('Should submit when click the button', () => {
    
    const wrapper = shallow(<AddNewApp />);
    const instance = wrapper.instance();
    jest.spyOn(instance, 'handleSubmit');
    wrapper.find('Button[type="submit"]').simulate('click');
    
    // make sure that we empty the input after the click
    expect(wrapper.state('appName')).toBe(""); 
});


it("", function () {
    
})