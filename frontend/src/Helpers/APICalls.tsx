import axios from "axios";

const API = {

    /**
     * Deleting API of App Version
     * @param appId
     * @param appVer
     */
    deleteAppVersion: (appId: string, appVer: string) => axios.delete(`/api/apps/${appId}/${appVer}`),

    /**
     * Get One App by ID
     */
    getAppByID : (appId:string) => axios.get(`/api/apps/${appId}`),

    /**
     * Delete One App By Sending His ID
     * @param appId
     */
    deleteAppByID : (appId:string) => axios.delete(`/api/apps/${appId}`)
};


export default API;